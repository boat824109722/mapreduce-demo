package com.boat;

import java.io.IOException;
import java.net.URI;
import java.util.StringTokenizer;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import com.boat.HDFSWordCount.MyMapper;
import com.boat.HDFSWordCount.MyReducer;

/**
 * @Description: MapReduce，输入文件和输出文件在本地磁盘上 
 * @author boat
 * @date 2018年1月26日 上午9:26:51
 * @version V1.0
 */
public class LocalWordCount {

	public static class WordCountMap extends Mapper<LongWritable, Text, Text, IntWritable> {
		private final IntWritable one = new IntWritable(1);
		private Text word = new Text();

		public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
			String line = value.toString();
			StringTokenizer token = new StringTokenizer(line);
			while (token.hasMoreTokens()) {
				word.set(token.nextToken());
				context.write(word, one);
			}
		}
	}

	public static class WordCountReduce extends Reducer<Text, IntWritable, Text, IntWritable> {
		public void reduce(Text key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
			int sum = 0;
			for (IntWritable val : values) {
				sum += val.get();
			}
			context.write(key, new IntWritable(sum));
		}
	}

	/**
	 * 项目路径下
	 */
	static final String INPUT_PATH = "./input/word";
	static final String OUT_PATH = "./output";

	public static void main(String[] args) throws Exception {
		try {
			Configuration conf = new Configuration();
			Job job = Job.getInstance(conf, LocalWordCount.class.getSimpleName());

			// 1.1指定读取的文件位于哪里
			FileInputFormat.setInputPaths(job, INPUT_PATH);
			// 指定如何对输入的文件进行格式化，把输入文件每一行解析成键值对
			// job.setInputFormatClass(TextInputFormat.class);

			// 1.2指定自定义的map类
			job.setMapperClass(MyMapper.class);
			// map输出的<k,v>类型。如果<k3,v3>的类型与<k2,v2>类型一致，则可以省略
			// job.setOutputKeyClass(Text.class);
			// job.setOutputValueClass(LongWritable.class);

			// 1.3分区
			// job.setPartitionerClass(HashPartitioner.class);

			// 1.4排序、分组

			// 1.5归约

			// 2.2指定自定义reduce类
			job.setReducerClass(MyReducer.class);
			// 有一个reduce任务运行
			// job.setNumReduceTasks(1);
			// 指定reduce的输出类型
			job.setOutputKeyClass(Text.class);
			job.setOutputValueClass(LongWritable.class);

			// 2.3指定写出到哪里
			// 如果输出路径已经存在，要先删除输出路径
			FileSystem fileSystem = FileSystem.get(new URI(OUT_PATH), conf);
			Path outPath = new Path(OUT_PATH);
			if (fileSystem.exists(outPath)) {
				fileSystem.delete(outPath, true);
			}
			FileOutputFormat.setOutputPath(job, outPath);
			// 指定输出文件的格式化类
			// job.setOutputFormatClass(TextOutputFormat.class);
			// 把job提交给jobtracker运行
			job.waitForCompletion(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}