package com.boat;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.net.URI;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.mapreduce.lib.partition.HashPartitioner;

/**
 * @Description: 复写了输出值，让mapreduce可以统计到更多的内容，当然还有很多种方式可以做到 
 * @author boat 
 * @date 2018年1月29日 上午9:11:04 
 * @version V1.0
 */
class PhoneCallWritable implements Writable {
	/**
	 * 通话时长
	 */
	long callTime;
	/**
	 * 通话前等待时长
	 */
	long waitTime;
	/**
	 * 通话次数
	 */
	long times;

	public PhoneCallWritable() {
	}

	public PhoneCallWritable(String callTime, String waitTime, String times) {
		this.callTime = Long.parseLong(callTime);
		this.waitTime = Long.parseLong(waitTime);
		this.times = Long.parseLong(times);
	}

	public void readFields(DataInput in) throws IOException {
		this.callTime = in.readLong();
		this.waitTime = in.readLong();
		this.times = in.readLong();
	}

	public void write(DataOutput out) throws IOException {
		out.writeLong(callTime);
		out.writeLong(waitTime);
		out.writeLong(times);
	}

	@Override
	public String toString() {
		return callTime + "\t" + waitTime + "\t" + times;
	}
}

public class LocalOverrideWritable {

	static class PhoneCallPartitioner extends HashPartitioner<Text, PhoneCallWritable> {
		@Override
		public int getPartition(Text key, PhoneCallWritable value, int numReduceTasks) {
			return (key.toString().substring(0, 1).equals("1")) ? 0 : 1;// 0代表的是手机号码\1代表非手机号码
		}
	}

	static class MyMapper extends Mapper<LongWritable, Text, Text, PhoneCallWritable> {
		protected void map(LongWritable key, Text value, org.apache.hadoop.mapreduce.Mapper<LongWritable, Text, Text, PhoneCallWritable>.Context context) throws IOException, InterruptedException {
			String[] splited = value.toString().split("\t");
			Text key2 = new Text(splited[0] + "\t" + splited[1]);
			PhoneCallWritable value2 = new PhoneCallWritable(splited[2], splited[3], "1");
			context.write(key2, value2);
		};
	}

	static class MyReducer extends Reducer<Text, PhoneCallWritable, Text, PhoneCallWritable> {
		protected void reduce(Text key, Iterable<PhoneCallWritable> values, Reducer<Text, PhoneCallWritable, Text, PhoneCallWritable>.Context context) throws IOException, InterruptedException {
			long callTime = 0L;
			long waitTime = 0L;
			long times = 0L;
			for (PhoneCallWritable phoneCallWritable : values) {
				callTime += phoneCallWritable.callTime;
				waitTime += phoneCallWritable.waitTime;
				times += phoneCallWritable.times;
			}
			final PhoneCallWritable value = new PhoneCallWritable(callTime + "", waitTime + "", times + "");
			context.write(key, value);
		};
	}

	/**
	 * 项目路径下
	 */
	static final String INPUT_PATH = "./input/phoneCall";
	static final String OUT_PATH = "./output";

	public static void main(String[] args) throws Exception {
		// hadoop配置文件
		Configuration conf = new Configuration();
		// 任务，将配置文件装载到任务中
		Job job = Job.getInstance(conf);
		job.setJarByClass(LocalOverrideWritable.class);

		// 1.1 指定输入文件路径
		FileInputFormat.setInputPaths(job, INPUT_PATH);
		// 指定哪个类用来格式化输入文件
		job.setInputFormatClass(TextInputFormat.class);

		// 1.2指定自定义的Mapper类
		job.setMapperClass(MyMapper.class);
		// 指定输出<k2,v2>的类型
		job.setMapOutputKeyClass(Text.class);
		job.setMapOutputValueClass(PhoneCallWritable.class);

		// 1.3 指定分区类
		job.setPartitionerClass(PhoneCallPartitioner.class);
		job.setNumReduceTasks(2);// 分成两个区

		// 1.4 TODO 排序、分区
		// 1.5 TODO （可选）归约

		// 2.2 指定自定义的reduce类
		job.setReducerClass(MyReducer.class);
		// 指定输出<k3,v3>的类型
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(PhoneCallWritable.class);

		// 2.3 指定输出到哪里
		// 如果输出路径已经存在，要先删除输出路径s
		FileSystem fileSystem = FileSystem.get(new URI(OUT_PATH), conf);
		Path outPath = new Path(OUT_PATH);
		if (fileSystem.exists(outPath)) {
			fileSystem.delete(outPath, true);
		}
		FileOutputFormat.setOutputPath(job, new Path(OUT_PATH));
		// 设定输出文件的格式化类
		job.setOutputFormatClass(TextOutputFormat.class);

		// 把代码提交给JobTracker执行
		job.waitForCompletion(true);
	}

}