package com.boat;

import java.net.URI;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

/**
 * @Description: MapReduce应用，输入文件和输出文件在HDFS文件系统上 
 * @author boat
 * @date 2018年1月26日 上午9:22:25
 * @version V1.0
 */
public class HDFSWordCount {
	static class MyMapper extends Mapper<LongWritable, Text, Text, LongWritable> {
		protected void map(LongWritable k1, Text v1, Context context) throws java.io.IOException, InterruptedException {
			String[] splited = v1.toString().split(" ");
			for (String word : splited) {
				context.write(new Text(word), new LongWritable(1));
			}
		};
	}

	static class MyReducer extends Reducer<Text, LongWritable, Text, LongWritable> {
		protected void reduce(Text k2, java.lang.Iterable<LongWritable> v2s, Context ctx) throws java.io.IOException, InterruptedException {
			long times = 0L;
			for (LongWritable count : v2s) {
				times += count.get();
			}
			ctx.write(k2, new LongWritable(times));
		};
	}

	/**
	 * 在HDFS目录下创建/input和把数据放到该目录下
	 */
	static final String INPUT_PATH = "hdfs://192.168.100.60:8020/input";
	static final String OUT_PATH = "hdfs://192.168.100.60:8020/output";

	public static void main(String[] args) {
		try {
			Configuration conf = new Configuration();
			Job job = Job.getInstance(conf, HDFSWordCount.class.getSimpleName());

			// 1.1指定读取的文件位于哪里
			FileInputFormat.setInputPaths(job, INPUT_PATH);
			// 指定如何对输入的文件进行格式化，把输入文件每一行解析成键值对
			// job.setInputFormatClass(TextInputFormat.class);

			// 1.2指定自定义的map类
			job.setMapperClass(MyMapper.class);
			// map输出的<k,v>类型。如果<k3,v3>的类型与<k2,v2>类型一致，则可以省略
			// job.setOutputKeyClass(Text.class);
			// job.setOutputValueClass(LongWritable.class);

			// 1.3分区
			// job.setPartitionerClass(HashPartitioner.class);

			// 1.4排序、分组

			// 1.5归约

			// 2.2指定自定义reduce类
			job.setReducerClass(MyReducer.class);
			// 有一个reduce任务运行
			// job.setNumReduceTasks(1);
			// 指定reduce的输出类型
			job.setOutputKeyClass(Text.class);
			job.setOutputValueClass(LongWritable.class);

			// 2.3指定写出到哪里
			// 如果输出路径已经存在，要先删除输出路径
			FileSystem fileSystem = FileSystem.get(new URI(OUT_PATH), conf);
			Path outPath = new Path(OUT_PATH);
			if (fileSystem.exists(outPath)) {
				fileSystem.delete(outPath, true);
			}
			FileOutputFormat.setOutputPath(job, outPath);
			// 指定输出文件的格式化类
			// job.setOutputFormatClass(TextOutputFormat.class);
			// 把job提交给jobtracker运行
			job.waitForCompletion(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}