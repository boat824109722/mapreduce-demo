package com.boat;

import java.io.IOException;
import java.net.URI;
import java.util.StringTokenizer;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.mapreduce.lib.partition.HashPartitioner;

/**
 * @Description: mapreduce进行分区，这样可以达到分类处理数据，并把结果分开输出，reduce的Task数量必须大于等于分区数 
 * @author boat 
 * @date 2018年1月29日 上午9:17:22 
 * @version V1.0
 */
public class LocalWordCountPartition {
	static final String INPUT_PATH = "./input/phone";
	static final String OUT_PATH = "./output";

	static class KpiPartitioner extends HashPartitioner<Text, IntWritable> {
		@Override
		public int getPartition(Text key, IntWritable value, int numReduceTasks) {
			return (key.toString().length() == 11) ? 0 : 1;// 0代表的是手机号码 1代表非手机号码
		}
	}

	static class MyMapper extends Mapper<LongWritable, Text, Text, IntWritable> {
		private final IntWritable one = new IntWritable(1);
		private Text word = new Text();

		protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
			String line = value.toString();
			StringTokenizer token = new StringTokenizer(line);
			while (token.hasMoreTokens()) {
				word.set(token.nextToken());
				context.write(word, one);
			}
		};
	}

	static class MyReducer extends Reducer<Text, IntWritable, Text, IntWritable> {
		public void reduce(Text key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
			int sum = 0;
			for (IntWritable val : values) {
				sum += val.get();
			}
			context.write(key, new IntWritable(sum));
		}
	}

	public static void main(String[] args) {
		try {
			Configuration conf = new Configuration();
			Job job = Job.getInstance(conf, LocalWordCountPartition.class.getSimpleName());

			// 1.1指定读取的文件位于哪里
			FileInputFormat.setInputPaths(job, INPUT_PATH);
			// 指定如何对输入的文件进行格式化，把输入文件每一行解析成键值对
			job.setInputFormatClass(TextInputFormat.class);

			// 1.2指定自定义的map类
			job.setMapperClass(MyMapper.class);
			// map输出的<k,v>类型。如果<k3,v3>的类型与<k2,v2>类型一致，则可以省略
			job.setOutputKeyClass(Text.class);
			job.setOutputValueClass(IntWritable.class);

			// 1.3分区
			job.setPartitionerClass(KpiPartitioner.class);
			job.setNumReduceTasks(2);// 分成两个区

			// 1.4排序、分组

			// 1.5归约

			// 2.2指定自定义reduce类
			job.setReducerClass(MyReducer.class);

			// 指定reduce的输出类型
			job.setOutputKeyClass(Text.class);
			job.setOutputValueClass(IntWritable.class);

			// 2.3指定写出到哪里
			// 如果输出路径已经存在，要先删除输出路径
			FileSystem fileSystem = FileSystem.get(new URI(OUT_PATH), conf);
			Path outPath = new Path(OUT_PATH);
			if (fileSystem.exists(outPath)) {
				fileSystem.delete(outPath, true);
			}
			FileOutputFormat.setOutputPath(job, outPath);
			// 指定输出文件的格式化类
			job.setOutputFormatClass(TextOutputFormat.class);
			// 把job提交给jobtracker运行
			job.waitForCompletion(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}